// Join URL: https://discordapp.com/api/oauth2/authorize?response_type=code&client_id=xxxxx&scope=bot&redirect_uri=https://myuri.com/

if (!console)
	console = {};
if (!console.log)
	console.log = () => {};

function Bot(token_elem_id, user_id_elem_id)
{
	this.OP_DISPATCH = 0;
	this.OP_HEARTBEAT = 1;
	this.OP_IDENT = 2;
	this.OP_HELLO = 10;
	this.OP_HEARTBEAT_ACK = 11;

	this.STATE_INIT_GATEWAY = 0;
	this.STATE_INIT_WS = 1;
	this.STATE_CONNECTED = 2;

	this.api_http_version = 6;
	this.api_gateway_version = 6;
	this.api_root = 'https://discordapp.com/api';

	this.state = this.STATE_INIT_GATEWAY;
	this.api_gateway = null;
	this.ws = null;
	this.last_seq = null;
	this.heartbeat_interval = 20000;
	this.heartbeat_interval_id = 0;

	let token_elem = document.getElementById(token_elem_id);
	this.token = token_elem.value;

	let user_id_elem = document.getElementById(user_id_elem_id);
	this.user_id = user_id_elem.value;

	this.initGateway();
}

Bot.prototype.prepare_api_rq = function(method, url, data)
{
	let rq = new APIRequest(method, this.api_root + '/v' + this.api_http_version + url, data);

	rq.setHeader('Authorization', 'Bot ' + this.token);

	return rq;
}

Bot.prototype.initGateway = function()
{
	let rq;

	if (this.state !== this.STATE_INIT_GATEWAY)
	{
		console.log('Internal error: expected state ' + this.STATE_INIT_GATEWAY + ', got ' + this.state + '.');
		return;
	}

	rq = new APIRequest('GET', this.api_root + '/gateway', null);

	rq.onsuccess = (req, evt) => {
		try
		{
			let response = JSON.parse(req.responseText);
			if (typeof response.url === 'undefined')
				throw 'Missing url param.';

			this.api_gateway = response.url;
		}
		catch (ex)
		{
			console.log('Unable to decode the gateway answer: ' + ex);
			return;
		}

		console.log('Received gateway: ' + this.api_gateway);

		this.state = this.STATE_INIT_WS;
		this.initWS();
	};

	rq.onerror = (req, evt) => {
		console.log('An error ' + req.status + ' (' + req.statusText + ') occurred while fetching the gateway.');
	};

	rq.send();
}

Bot.prototype.send_op = function (op, data)
{
	let d = {};

	d.op = op;

	if (typeof data !== 'undefined')
		d.d = data;

	d = JSON.stringify(d);

	console.log('ws_send(' + d + ')');
	return this.ws.send(d);
};

Bot.prototype.send_heartbeat = function ()
{
	return this.send_op(this.OP_HEARTBEAT, this.last_seq);
};

Bot.prototype.send_ident = function (tok, props, presence)
{
	return this.send_op(this.OP_IDENT, {
		token: tok,
		properties: props,
		presence: presence
	});
};

Bot.prototype.api_delete_message = function(id_chan, id_msg) {
	let rq = this.prepare_api_rq('DELETE', '/channels/' + id_chan + '/messages/' + id_msg, null);

	rq.onsuccess = (req, evt) => {
		console.log('Deleted message #' + id_msg + ' from channel #' + id_chan + '.');
	};

	rq.onerror = (req, evt) => {
		console.log('Failed to delete message #' + id_msg + ' from channel #' + id_chan + ': ' + req.status + '(' + req.statusText +').');
	};

	rq.send();
}

Bot.prototype.api_create_message = function(id_chan, msg) {
	let data = JSON.stringify(msg);
	let rq = this.prepare_api_rq('POST', '/channels/' + id_chan + '/messages', data);

	rq.setHeader('Content-Type', 'application/json');

	rq.onsuccess = (req, evt) => {
		console.log('Created message in channel #' + id_chan + '.');
	};

	rq.onerror = (req, evt) => {
		console.log('Failed to create message in channel #' + id_chan + ': ' + req.status + '(' + req.statusText +').');
	};

	rq.send();
}

Bot.prototype.initWS = function()
{
	if (this.state !== this.STATE_INIT_WS)
	{
		console.log('Internal error: expected state ' + this.STATE_INIT_WS + ', got ' + this.state + '.');
		return;
	}

	this.ws = new WebSocket(this.api_gateway + '?v=' + this.api_gateway_version + '&encoding=json');

	this.ws.onopen = (ev) => {
		console.log('ws_open()');
	};

	this.ws.onmessage = (ev) => {
		let data;
		let d;

		try
		{
			data = JSON.parse(ev.data);
		}
		catch (ex)
		{
			console.log('Invalid msg: ' + ex + ': ' + ev.data + ')');
			return;
		}

		d = data.d;

		console.log('ws_msg(' + ev.data + ')');

		switch (data.op)
		{
			case this.OP_DISPATCH:
				this.last_seq = data.s;

				console.log('DISPATCH.' + data.t);

				switch (data.t)
				{
					case 'READY':
						this.state = this.STATE_CONNECTED;
						break;

					case 'MESSAGE_CREATE':
					case 'MESSAGE_UPDATE':
						let msg = d;

						if (msg.author.id !== this.user_id)
							break;

						let r_sym = '-:`${}()@';
						let rgx_sym = '[' + r_sym + ']+';
						let rgx_sep = '[^a-zA-Z0-9]{0,10}';
						let rgx_sep_end = '[^' + r_sym + 'a-zA-Z0-9]{0,10}';
						let rgx_smi_mw = (words) => {
							let r = rgx_sym;
							for (let w of words)
								r += rgx_sep + w;
							return new RegExp(r + rgx_sep_end + rgx_sym, 'g');
						}

						let content = msg.content.replace(/\\<:\S+:\d+>|\\:\S+:|:\S+\\:|(^|[^<]):\S+:/g, '$1<:calim:295578280004681738>');
						if (content !== msg.content)
							content = content.replace(/(`{1,3})([^`]+)\1/g, '$2');
						content = content.replace(rgx_smi_mw([ 'in(?:t?r?|rt)u(?:s|z)ion', 'cc?ard', '50' ]), '<:calim:295578280004681738>');
						content = content.replace(rgx_smi_mw([ 'h?ordes?', 'lol' ]), '<:smile:453636661205794817>');
						if (content !== msg.content)
							content = content.replace(/(`{1,3})([^`]+)\1/g, '$2');
						if (content === msg.content)
							break;

						msg.content = content;

						this.api_create_message(msg.channel_id, msg);
						this.api_delete_message(msg.channel_id, msg.id);
						break;
				}
				break;

			case this.OP_HEARTBEAT:
				console.log('HEARTBEAT');
				this.send_heartbeat();
				break;

			case this.OP_HEARTBEAT_ACK:
				console.log('HEARTBEAT_ACK');
				break;

			case this.OP_HELLO:
				console.log('HELLO');
				this.heartbeat_interval = d.heartbeat_interval;

				this.heartbeat_interval_id = window.setInterval(() => {
					this.send_heartbeat();
				}, this.heartbeat_interval);

				this.send_ident(this.token, {}, {
					status: 'invisible',
					afk: false
				});
				break;
		}
	};

	this.ws.onerror = (ev) => {
		console.log('ws_err()');
	};

	this.ws.onclose = (ev) => {
		console.log('ws_close(' + ev.code + ')');

		window.clearInterval(this.heartbeat_interval_id);

		this.state = this.STATE_INIT_GATEWAY;
		this.initGateway();
	};
}

function APIRequest(method, url, data)
{
	this.method = method;
	this.url = url;
	this.data = data;
	this.headers = {};
	this.rq = null;
	this.onsuccess = () => {};
	this.onerror = () => {};
}

APIRequest.prototype.setHeader = function(k, v)
{
	this.headers[k] = v;
};

APIRequest.prototype.send = function()
{
	this.rq = new XMLHttpRequest();

	this.rq.onreadystatechange = (evt) => {
		if (this.rq.readyState !== XMLHttpRequest.DONE)
			return;

		if (this.rq.status !== 200
				&& this.rq.status !== 204)
		{
			this.rq.onerror(evt);
			return;
		}

		this.onsuccess(this.rq, evt);
	};

	this.rq.onerror = (evt) => {
		this.onerror(this.rq, evt);
	};

	this.rq.open(this.method, this.url, true);

	for (let k in this.headers)
	{
		if (!this.headers.hasOwnProperty(k))
			continue;

		this.rq.setRequestHeader(k, this.headers[k]);
	}

	this.rq.send(this.data);
};
